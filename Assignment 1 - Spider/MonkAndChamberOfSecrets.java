import java.util.*;

class MonkAndChamberOfSecrets {

    private static class Spider {
        int powerOfSpider;
        int positionOfSpider;
    }

    public static Queue<Spider>  inputSpiderQueue(int noOfSpiders, Scanner input) {
        Queue<Spider> queueOfSpiders = new LinkedList<>();

        for (int indexOfSpiderInQueue = 1; indexOfSpiderInQueue <= noOfSpiders; indexOfSpiderInQueue++){
            Spider spider = new Spider();
            spider.powerOfSpider = input.nextInt();
            spider.positionOfSpider = indexOfSpiderInQueue;
            queueOfSpiders.add(spider);
        }
        return queueOfSpiders;
    }

    public static void findAndPrintTheIndexesOfTheSpidersToBeSelected(Queue<Spider> queueOfSpiders, int noOfSpidersToBeDequed) {

        Spider[] dequedSpiders = new Spider[noOfSpidersToBeDequed];

         for (int noOfSpidersSelected = 0; noOfSpidersSelected < noOfSpidersToBeDequed; noOfSpidersSelected++){ 
            int noOfSpidersDequed = 0;
            int indexOfSpiderWithMaximumPower = 0;
            
            while (!queueOfSpiders.isEmpty() && noOfSpidersDequed < noOfSpidersToBeDequed){
                Spider removedSpider = queueOfSpiders.remove();
                dequedSpiders[noOfSpidersDequed] = removedSpider;
                if (removedSpider.powerOfSpider > dequedSpiders[indexOfSpiderWithMaximumPower].powerOfSpider){
                    indexOfSpiderWithMaximumPower = noOfSpidersDequed;
                }
                noOfSpidersDequed++;
            }
            
            System.out.print(dequedSpiders[indexOfSpiderWithMaximumPower].positionOfSpider + " ");
            int noOfSpidersToBeEnqued = noOfSpidersDequed - 1;
            
            for (int indexOfDequedSpiders = 0 ; indexOfDequedSpiders <= noOfSpidersToBeEnqued; indexOfDequedSpiders++){
                if (dequedSpiders[indexOfDequedSpiders].powerOfSpider > 0){
                    dequedSpiders[indexOfDequedSpiders].powerOfSpider -= 1;
                }
                if (indexOfDequedSpiders != indexOfSpiderWithMaximumPower){
                    queueOfSpiders.add(dequedSpiders[indexOfDequedSpiders]);
                }
            }
        }
    }

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int noOfSpiders = input.nextInt();
        int noOfSpidersToBeDequed = input.nextInt();

        Queue<Spider> queueOfSpiders = inputSpiderQueue(noOfSpiders, input);

        findAndPrintTheIndexesOfTheSpidersToBeSelected(queueOfSpiders, noOfSpidersToBeDequed);
    }
}